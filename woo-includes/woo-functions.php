<?php
/**
 * Functions used by plugins
 */

if ( ! class_exists( 'WC_Dependencies' ) ) require_once( 'class-wc-dependencies.php' );

/**
 * WC Detection
 **/
function is_woocommerce_active() {

	return WC_Dependencies::woocommerce_active_check();
	
}

add_action( 'admin_enqueue_scripts', 'wc_ie_plugin_scripts' );
function wc_ie_plugin_scripts(){
	wp_enqueue_script('wc-ie-script', plugins_url() . '/woocommerce-invoicexpress/woo-includes/lib/script.js', array(), '1.0.0', false);
}


//Add field to checkout
add_filter('woocommerce_checkout_fields' , 'wc_ie_nif_checkout');
function wc_ie_nif_checkout( $fields ) {
	global $woocommerce;
	if(trim($woocommerce->customer->get_country())=='PT') {
		$current_user=wp_get_current_user();
		$fields['billing']['billing_nif'] = array(
			'type'			=>	'text',
			'label'			=> __('NIF / NIPC', 'woocommerce_nif'),
			'placeholder'	=> _x('Portuguese VAT identification number', 'placeholder', 'woocommerce_nif'),
			'class'			=> array('form-row-first'),
			'required'		=> false,
			'clear'			=> true,
			'default'		=> ($current_user->billing_nif ? trim($current_user->billing_nif) : ''),
		);
	}
	return $fields;
}

//Add NIF to My Account / Billing Address form
add_filter('woocommerce_address_to_edit', 'wc_ie_nif_my_account');
function wc_ie_nif_my_account( $fields ) {
	global $wp_query;
	if (isset($wp_query->query_vars['edit-address']) && $wp_query->query_vars['edit-address']!='billing') {
		return $fields;
	} else {
		$current_user=wp_get_current_user();
		if ($current_user->billing_country=='PT') {
			$fields['billing_nif']=array(
				'type'			=>	'text',
				'label'			=> __('NIF / NIPC', 'woocommerce_nif'),
				'placeholder'	=> _x('Portuguese VAT identification number', 'placeholder', 'woocommerce_nif'),
				'class'			=> array('form-row-first'),
				'required'		=> false,
				'clear'			=> true,
				'default'		=> ($current_user->billing_nif ? trim($current_user->billing_nif) : ''),
			);
		}
		return $fields;
	}
}

//Save NIF to customer Billing Address
add_action('woocommerce_customer_save_address', 'wc_ie_my_account_save', 10, 2);
function wc_ie_my_account_save($user_id, $load_address) {
	if ($load_address=='billing') {
		if (isset($_POST['billing_nif'])) {
			update_user_meta( $user_id, 'billing_nif', trim($_POST['billing_nif']) );
		}
	}
}

//Add field to order admin panel
add_action( 'woocommerce_admin_order_data_after_billing_address', 'wc_ie_nif_admin', 10, 1);
function wc_ie_nif_admin($order){
	if (@is_array($order->order_custom_fields['_billing_country'])) {
		//Old WooCommerce versions
		if(@in_array('PT', $order->order_custom_fields['_billing_country']) ) {
			echo "<p><strong>".__('NIF / NIPC', 'woocommerce_nif').":</strong> " . $order->order_custom_fields['_billing_nif'][0] . "</p>";
  		}
	} else {
		//New WooCommerce versions
		if ($order->billing_country=='PT') {
			$order_custom_fields=get_post_custom($order->ID);
			echo "<p><strong>".__('NIF / NIPC', 'woocommerce_nif').":</strong> " . $order_custom_fields['_billing_nif'][0] . "</p>";
		}
	}
}

function  wc_ie_get_core_info() {
		
	global $wpdb;

	$core = array(
		'Wordpress' => array(
			'Multisite'          => is_multisite() ? 'Yes' : 'No',
			'SiteURL'            => site_url(),
			'HomeURL'            => home_url(),
			'Version'            => get_bloginfo( 'version' ),
			'PermalinkStructure' => get_option( 'permalink_structure' ),
			'PostTypes'          => implode( ', ', get_post_types( '', 'names' ) ),
			'PostSatus'          => implode( ', ', get_post_stati() )
		),
		'Server'    => array(
			'jQueryVersion'  => wp_script_is( 'jquery', 'registered' ) ? $GLOBALS[ 'wp_scripts' ]->registered[ 'jquery' ]->ver : __( 'n/a', 'bbpress' ),
			'PHPVersion'     => phpversion(),
			'MySQLVersion'   => $wpdb->db_version(),
			'ServerSoftware' => $_SERVER[ 'SERVER_SOFTWARE' ]
		),
		'PHP'       => array(
			'MemoryLimit'  => ini_get( 'memory_limit' ),
			'UploadMax'    => ini_get( 'upload_max_filesize' ),
			'PostMax'      => ini_get( 'post_max_size' ),
			'TimeLimit'    => ini_get( 'max_execution_time' ),
			'MaxInputVars' => ini_get( 'max_input_vars' ),
		),
	);

	return $core;
}

function wc_ie_get_plugins_info() {
	
	if ( ! function_exists( 'get_plugins' ) ) {
		$admin_includes_path = str_replace( site_url('/', 'admin'), ABSPATH, admin_url('includes/', 'admin') );
		require_once $admin_includes_path . 'plugin.php';
	}
	
	$plugins = get_plugins();
	$active_plugins = get_option('active_plugins');
	$active_plugins_info = array();
	foreach ($active_plugins as $plugin) {
		if (isset($plugins[$plugin])) {
			unset($plugins[$plugin]['Description']);
			$active_plugins_info[$plugin] = $plugins[$plugin];
		}
	}
	
	$mu_plugins = get_mu_plugins();

	$dropins = get_dropins();

	$output =array(
		'active_plugins' => $active_plugins_info,
		'mu_plugins' => $mu_plugins,
		'dropins' => $dropins,
	);
	
	return $output;
}

function wc_ie_get_theme_info() {
	
	/** @var WP_Theme $current_theme */
	if ( get_bloginfo( 'version' ) < '3.4' ) {
		$current_theme = get_theme_data( get_stylesheet_directory() . '/style.css' );
		$theme = $current_theme;
		unset($theme['Description']);
		unset($theme['Satus']);
		unset($theme['Tags']);
	} else {
		$current_theme = wp_get_theme();
		$theme = array(
			'Name'       => $current_theme->Name,
			'ThemeURI'   => $current_theme->ThemeURI,
			'Author'     => $current_theme->Author,
			'AuthorURI'  => $current_theme->AuthorURI,
			'Template'   => $current_theme->Template,
			'Version'    => $current_theme->Version,
			'TextDomain' => $current_theme->TextDomain,
			'DomainPath' => $current_theme->DomainPath,
		);
	}
	
	return $theme;
}


